Manage point database

```
use manage_point;
create table SINHVIEN (
	MASV int NOT NULL,
    HODEM varchar(45),
    TEN varchar(45),
    NGAYSINH date,
    GIOITINH double,
    NOISINH varchar(45),
    MALOP varchar(45),
    PRIMARY KEY (MASV)
);

create table MONHOC (
	MAMONHOC int NOT NULL,
    TENMONHOC varchar(45),
    SODVHT varchar(45),
    PRIMARY KEY (MAMONHOC)
);

create table DIEMTHI (
 MASV int NOT NULL,
 MAMONHOC int NOT NULL,
 DIEMLAN1 float,
 DIEMLAN2 float,
 FOREIGN KEY (MASV) REFERENCES SINHVIEN(MASV),
 FOREIGN KEY (MAMONHOC) REFERENCES MONHOC(MAMONHOC)
)
```
